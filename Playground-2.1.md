# Playground 2.0 Real

1. Creamos el archivo del deployment:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello-deploy
spec:
  replicas: 10
  selector:
    matchLabels:
      app: hello-world
  revisionHistoryLimit: 5
  progressDeadlineSeconds: 300
  minReadySeconds: 10
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 1
      maxSurge: 1
  template:
    metadata:
      labels:
        app: hello-world
    spec:
      containers:
      - name: hello-pod
        image: nigelpoulton/k8sbook:1.0
        ports:
        - containerPort: 8080
```

2. Creamos y aplicamos el deployment.

```yaml
[node1 deployments]$ kubectl apply -f deploy.yml
deployment.apps/hello-deploy created
```

3. Inspect el deployment:

```yaml
***$ kubectl get deploy hello-deploy***
NAME           READY   UP-TO-DATE   AVAILABLE   AGE
hello-deploy   10/10   10           10          4m17s

***$ kubectl describe deploy hello-deploy***
Name:                   hello-deploy
Namespace:              default
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               app=hello-world
Replicas:               10 desired | 10 updated | 10 total | 10 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        10
RollingUpdateStrategy:  1 max unavailable, 1 max surge
Pod Template:
  Labels:  app=hello-world
  Containers:
   hello-pod:
    Image:        nigelpoulton/k8sbook:1.0
    Port:         8080/TCP
<SNIP>
OldReplicaSets:  <none>
NewReplicaSet:   hello-deploy-67959776b5 (10/10 replicas created)
```

4. Validemos los replica set.

```yaml
***$ kubectl get rs***
NAME                      DESIRED   CURRENT   READY   AGE
hello-deploy-67959776b5   10        10        10      6m
```

5. Para ver mas detalles:

```yaml
***$ kubectl describe rs hello-deploy-67959776b5***
Name:           hello-deploy-67959776b5
Namespace:      default
Selector:       app=hello-world,pod-template-hash=67959776b5
Labels:         app=hello-world
                pod-template-hash=67959776b5
Annotations:    deployment.kubernetes.io/desired-replicas: 10
                deployment.kubernetes.io/max-replicas: 11
                deployment.kubernetes.io/revision: 1
Controlled By:  Deployment/hello-deploy
Replicas:       10 current / 10 desired
Pods Status:    10 Running / 0 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  app=hello-world
           pod-template-hash=67959776b5
  Containers:
   hello-pod:
    Image:        nigelpoulton/k8sbook:1.0
    Port:         8080/TCP
<Snip>
```

6. Jugamos con las replicas:

```yaml
$ kubectl scale deploy hello-deploy --replicas 5
deployment.apps/hello-deploy scaled

$ kubectl get deploy hello-deploy
NAME           READY   UP-TO-DATE   AVAILABLE   AGE
hello-deploy   5/5     5            5           105m
```

<aside>
💡 Se hizo scale down en segundos!

</aside>

6. Le caigo encima nuevamente al deployment sin tema.

```yaml
$ kubectl apply -f deploy.yml
deployment.apps/hello-deploy configured

$ kubectl get deploy hello-deploy
NAME           READY   UP-TO-DATE   AVAILABLE   AGE
hello-deploy   10/10   10           10          109m
```

7. Vemos el rollout el proceso super bueno, cada 10 segundos segun configuracion.

```yaml
$ kubectl rollout status deployment hello-deploy
Waiting for deployment "hello-deploy" rollout... 4 out of 10 new replicas...
Waiting for deployment "hello-deploy" rollout... 4 out of 10 new replicas...
Waiting for deployment "hello-deploy" rollout... 6 out of 10 new replicas...
```

![Untitled](Playground%202%200%20Real%203fd4ae77bf184545b8c389c72759a5ba/Untitled.png)